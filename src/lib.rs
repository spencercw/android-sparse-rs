//! An implementation of Android's sparse file format.
//!
//! Enables reading and writing sparse images, as well as encoding from and
//! decoding to raw images:
//!
//! ```text
//!  --------               --------                -------
//! | sparse | --Reader--> | sparse | --Decoder--> | raw   |
//! | image  | <--Writer-- | blocks | <--Encoder-- | image |
//!  --------               --------                -------
//! ```

#![cfg_attr(not(feature = "std"), no_std)]
#![deny(missing_docs)]

extern crate alloc;

pub mod block;
pub mod read;
pub mod result;
pub mod write;

mod ext;
mod headers;
mod io;

pub use self::{
    block::Block,
    read::{Encoder, Reader},
    result::Result,
    write::{Decoder, Writer},
};
